//
//  ContactViewControllerTableViewController.swift
//  ContactApp
//
//  Created by Muhammad Firman Akbar on 8/17/17.
//  Copyright © 2017 firmanpro. All rights reserved.
//

import UIKit

class ContactViewControllerTableViewController: UITableViewController {

    var TAG = "Log contact list : "
    var contacts: [Contact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let fred = Contact(name: "Hani", phoneNumber: "021777774444")
//        contacts.append(fred)
//        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }

    //cellFor - 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
        
        let contact = contacts[indexPath.row]
        
        cell.textLabel?.text = contact.name
        cell.detailTextLabel?.text = contact.phoneNumber
        
        return cell
    }
    
    //unwind segue
    @IBAction func unwindToContactList(segue: UIStoryboardSegue){
//        guard let viewController = segue.source as? AddContactViewController else { return }
//        guard let name = viewController.txtInputName.text, let phoneNumber = viewController.txtInputPhoneNumber.text else { return }
//        let contact = Contact(name: name, phoneNumber: phoneNumber)
//        contacts.append(contact)
//        tableView.reloadData()
        if let viewController = segue.source as? AddContactViewController {
            guard let name = viewController.txtInputName.text, let phoneNumber = viewController.txtInputPhoneNumber.text else { return }
            let contact = Contact(name: name, phoneNumber: phoneNumber)
            if let indexPath = viewController.indexPathForContact {
                contacts[indexPath.row] = contact
            }else{
                contacts.append(contact)
            }
            tableView.reloadData()
        }else if let viewController = segue.source as? ContactDetailViewController {
            if viewController.isDeleted{
                guard let indexPath: IndexPath = viewController.indexPath else { return }
                contacts.remove(at: indexPath.row)
                tableView.reloadData()
            }
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "contactDetailSegue" {
//            guard let viewController = segue.destination as? ContactDetailViewController else { return }
//            guard let indexPath = tableView.indexPathForSelectedRow else { return }
//            let contact = contacts[indexPath.row]
//            viewController.contact = contact
            guard let navViewController = segue.destination as? UINavigationController else { return }
            guard let viewController = navViewController.topViewController as? ContactDetailViewController else { return }
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            let contact = contacts[indexPath.row]
            viewController.contact = contact
            viewController.indexPath = indexPath
        }
    }
    
}

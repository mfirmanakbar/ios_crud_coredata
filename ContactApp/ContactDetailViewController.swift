//
//  ContactDetailViewController.swift
//  ContactApp
//
//  Created by Muhammad Firman Akbar on 8/17/17.
//  Copyright © 2017 firmanpro. All rights reserved.
//

import UIKit

class ContactDetailViewController: UIViewController {

    var TAG = "Log contact detail : "
    var contact: Contact? = nil
    var isDeleted: Bool = false
    var indexPath: IndexPath? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(TAG + String(describing: contact?.name))
        print(TAG + String(describing: contact?.phoneNumber))
        txtName.text = contact?.name
        txtPhone.text = contact?.phoneNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtPhone: UILabel!

    @IBAction func done(_ sender: Any) {
        performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    @IBAction func deleteContact(_ sender: Any) {
        isDeleted = true
        performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editContactSegue" {
            guard let viewController = segue.destination as? AddContactViewController else { return }
            viewController.titleText = "Edit Contact"
            viewController.contact = contact
            viewController.indexPathForContact = self.indexPath!
        }
    }
    
}

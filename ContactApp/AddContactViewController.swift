//
//  AddContactViewController.swift
//  ContactApp
//
//  Created by Muhammad Firman Akbar on 8/17/17.
//  Copyright © 2017 firmanpro. All rights reserved.
//

import UIKit

class AddContactViewController: UIViewController {

    var titleText = "Add Contact"
    var contact: Contact? = nil
    var indexPathForContact: IndexPath? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtLabelTitle.text = titleText
        if let contact = contact {
            txtInputName.text = contact.name
            txtInputPhoneNumber.text = contact.phoneNumber
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet weak var txtLabelTitle: UILabel!
    @IBOutlet weak var txtInputName: UITextField!
    @IBOutlet weak var txtInputPhoneNumber: UITextField!
    
    @IBAction func saveAndClose(_ sender: Any) {
        performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    @IBAction func close(_ sender: Any) {
        //txtInputName.text = nil
        //txtInputPhoneNumber.text = nil
        performSegue(withIdentifier: "unwindToContactList", sender: self)
    }

}

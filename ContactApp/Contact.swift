//
//  Contact.swift
//  ContactApp
//
//  Created by Muhammad Firman Akbar on 8/17/17.
//  Copyright © 2017 firmanpro. All rights reserved.
//

import Foundation

struct Contact {
    let name: String
    let phoneNumber: String
}
